﻿using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoHelper;
using YouFlyModels;
using System.IO;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Data
{
    public class DbInitializer
    {
        public static void Initialize(Context context)
        {
            char[] splitAt = { ',' };
            string[] airportCodeLines = System.IO.File.ReadAllLines("murica.txt");

            context.Database.EnsureCreated();

            if (!context.Users.Any())
            {
                var admin = new User { Email = "admin@selu.edu", Password = Crypto.HashPassword("password"), IsAdmin = true };
                context.Users.Add(admin);
            }

            if (!context.Airports.Any())
            {
                foreach (string line in airportCodeLines)
                {
                    string[] words = line.Split(splitAt, System.StringSplitOptions.RemoveEmptyEntries);

                    string airportCode = words[4].Replace('"', ' ').Trim();
                    string airportName = words[1].Replace('"', ' ').Trim();
                    string airportCity = words[2].Replace('"', ' ').Trim();
                    double latitude;
                    double longitude;

                    try
                    {
                        latitude = Convert.ToDouble(words[6]);
                        longitude = Convert.ToDouble(words[7]);
                    }
                    catch(Exception e)
                    {
                        latitude = Convert.ToDouble(words[7]);
                        longitude = Convert.ToDouble(words[8]);
                    }
                    

                    var airport = new Airport
                    {
                        Code = airportCode,
                        Name = airportName,
                        City = airportCity,
                        Latitude = latitude,
                        Longitude = longitude
                    };
                    context.Airports.Add(airport);
                }
            }
            else
            {
                return;
            }
            context.SaveChanges();
        }
        public static async void Create([Bind("Code,Name,City,Latitude,Longitude")] Airport airport)
        {
            string myURL = "https://yfapig3.azurewebsites.net/api/airportapi/";
            var client = new HttpClient();
            var response = await client.PostAsJsonAsync(myURL, airport);
        }


    }
}
