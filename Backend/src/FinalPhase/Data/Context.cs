﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using YouFlyModels;

namespace Backend.Data
{
    public class Context:  DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Purchase>()
                .HasIndex(b => b.ConfirmationNumber).IsUnique();
            modelBuilder.Entity<User>()
                .HasIndex(b => b.Email).IsUnique();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<RemainingSpace> remainingSpaces { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        

        

    }
}
