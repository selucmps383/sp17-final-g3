﻿using Backend.Data;
using System.Collections.Generic;
using System.Linq;
using YouFlyModels;

namespace Backend.Models
{
    public class RemainingSpaceRepository : IRemainingSpaceRepository
    {
        private readonly Context context;

        public RemainingSpaceRepository(Context context)
        {
            this.context = context;
        }

        public IEnumerable<RemainingSpace> GetAll()
        {
            return context.remainingSpaces.ToList();
        }
        public void Add(RemainingSpace remainingSpace)
        {
            this.context.remainingSpaces.Add(remainingSpace);
            this.context.SaveChanges();
        }


        public RemainingSpace Find(int id)
        {
            return this.context.remainingSpaces.FirstOrDefault(t => t.Id == id);
        }
        public RemainingSpace FindByRouteId(int routeid)
        {
            return this.context.remainingSpaces.FirstOrDefault(t => t.RouteId == routeid);
        }
        public void Remove(int id)
        {
            var entity = this.context.remainingSpaces.First(t => t.Id == id);
            this.context.remainingSpaces.Remove(entity);
            this.context.SaveChanges();
        }
        public void Update(RemainingSpace remainingSpace)
        {
            this.context.remainingSpaces.Update(remainingSpace);
            this.context.SaveChanges();
        }
    }
}
