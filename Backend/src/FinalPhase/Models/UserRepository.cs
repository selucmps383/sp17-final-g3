﻿using Backend.Data;
using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using YouFlyModels;

namespace Backend.Models
{
    public class UserRepository : IUserRepository
    {
        private readonly Context context;

        public UserRepository(Context context)
        {
            this.context = context;
        }

        public IEnumerable<User> GetAll()
        {
            return this.context.Users.ToList();
        }

        public void Add(User user)
        {
            this.context.Users.Add(user);
            this.context.SaveChanges();
        }

        public User Find(int id)
        {
            return this.context.Users.FirstOrDefault(t => t.Id == id);
        }
        public User FindByEmail(string email)
        {
            return this.context.Users.FirstOrDefault(t => t.Email == email);
        }

        public void Remove(int id)
        {
            var entity = this.context.Users.First(t => t.Id == id);
            this.context.Users.Remove(entity);
            this.context.SaveChanges();
        }

        public void Update(User user)
        {
            context.Users.Update(user);
            context.SaveChanges();
        }
    }
}
