﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFlyModels;

namespace Backend.Models
{
    public interface IPurchaseRepository
    {
        void Add(Purchase purchase);
        IEnumerable<Purchase> GetAll();
        Purchase Find(int id);
        Purchase FindByConfirmationNumber(int confirmationnumber);
        Purchase FindByUserEmail(string useremail);
        Purchase FindByRouteId(int routeid);
        void Remove(int id);
        void Update(Purchase purchase);
    }
}
