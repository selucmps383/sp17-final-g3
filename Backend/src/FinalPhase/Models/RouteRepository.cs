﻿using Backend.Data;
using Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using YouFlyModels;

namespace Backend.Models
{
    public class RouteRepository : IRouteRepository
    {
        private readonly Context context;

        public RouteRepository(Context context)
        {
            this.context = context;
        }

        public IEnumerable<Route> GetAll()
        {
            return this.context.Routes.ToList();
        }

        public void Add(Route route)
        {
            this.context.Routes.Add(route);
            this.context.SaveChanges();
        }

        public Route Find(int id)
        {
            return this.context.Routes.FirstOrDefault(t => t.Id == id);
        }
        public Route FindByTime(int time)
        {
            return this.context.Routes.FirstOrDefault(t => t.TimeOfDay == time);
        }
        public Route FindByDay(string day)
        {
            return this.context.Routes.FirstOrDefault(t => t.DayOfWeek == day);
        }
        public Route FindByToId(string toid)
        {
            return this.context.Routes.FirstOrDefault(t => t.ToAirportId == toid);
        }
        public Route FindByFromId(string fromid)
        {
            return this.context.Routes.FirstOrDefault(t => t.FromAirportId == fromid);
        }

        public void Remove(int id)
        {
            var entity = this.context.Routes.First(t => t.Id == id);
            this.context.Routes.Remove(entity);
            this.context.SaveChanges();
        }

        public void Update(Route route)
        {
            this.context.Routes.Update(route);
            this.context.SaveChanges();
        }
    }
}
