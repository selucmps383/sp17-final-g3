﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFlyModels;

namespace Backend.Models
{
    public interface IAirportRepository
    {
        void Add(Airport airport);
        IEnumerable<Airport> GetAll();
        Airport Find(int id);
        Airport FindByCity(string city);
        Airport FindByName(string name);
        Airport FindByCode(string code);
        void Remove(int id);
        void Update(Airport airport);

    }
}
