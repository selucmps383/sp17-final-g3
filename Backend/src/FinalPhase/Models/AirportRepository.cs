﻿using Backend.Data;
using System.Collections.Generic;
using System.Linq;
using YouFlyModels;

namespace Backend.Models
{
    public class AirportRepository: IAirportRepository
    {
        private readonly Context context;
        
        public AirportRepository(Context context)
        {
            this.context = context;
        }

        public IEnumerable<Airport> GetAll()
        {
            return context.Airports.ToList();
        }
        public void Add(Airport airport)
        {
            this.context.Airports.Add(airport);
            this.context.SaveChanges();
        }

        public Airport Find(int id)
        {
            return this.context.Airports.FirstOrDefault(t => t.Id == id);
        }
        public Airport FindByCity(string city)
        {
            return this.context.Airports.FirstOrDefault(t => t.City == city);
        }
        public Airport FindByName(string name)
        {
            return this.context.Airports.FirstOrDefault(t => t.Name == name);
        }
        public Airport FindByCode(string code)
        {
            return this.context.Airports.FirstOrDefault(t => t.Code == code);
        }
        public void Remove(int id)
        {
            var entity = this.context.Airports.First(t => t.Id == id);
            this.context.Airports.Remove(entity);
            this.context.SaveChanges();
        }
        public void Update(Airport airport)
        {
            this.context.Airports.Update(airport);
            this.context.SaveChanges();
        }
    }
}
