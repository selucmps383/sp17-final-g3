﻿using System.Collections.Generic;
using YouFlyModels;

namespace Backend.Models
{
    public interface ITicketRepository
    {
        void Add(Ticket ticket);
        IEnumerable<Ticket> GetAll();
        Ticket Find(int id);
        Ticket FindByFirstName(string firstname);
        Ticket FindByLastName(string lastname);
        Ticket FindByPurchaseId(int purchaseId);
        void Remove(int id);
        void Update(Ticket ticket);
    }
}