﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFlyModels;

namespace Backend.Models
{
    public interface IRemainingSpaceRepository
    {
        void Add(RemainingSpace remainingSpace);
        IEnumerable<RemainingSpace> GetAll();
        RemainingSpace Find(int id);
        RemainingSpace FindByRouteId(int routeid);
        void Remove(int id);
        void Update(RemainingSpace remainingSpace);
    }
}
