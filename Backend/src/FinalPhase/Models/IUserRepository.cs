﻿using System.Collections.Generic;
using YouFlyModels;

namespace Backend.Models
{
    public interface IUserRepository
    {
        void Add(User user);
        IEnumerable<User> GetAll();
        User Find(int id);
        User FindByEmail(string email);
        void Remove(int id);
        void Update(User user);
    }
}
