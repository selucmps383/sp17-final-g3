﻿using Backend.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using YouFlyModels;

namespace Backend.Models
{
    public class TicketRepository : ITicketRepository
    {
        private readonly Context context;

        public TicketRepository(Context context)
        {
            this.context = context;
        }

        public IEnumerable<Ticket> GetAll()
        {
            return this.context.Tickets.ToList();
        }

        public void Add(Ticket ticket)
        {
            this.context.Tickets.Add(ticket);
            this.context.SaveChanges();
        }

        public Ticket Find(int id)
        {
            return this.context.Tickets.FirstOrDefault(t => t.Id == id);
        }
        public Ticket FindByFirstName(string firstname)
        {
            return this.context.Tickets.FirstOrDefault(t => t.OccupantFirstName == firstname);
        }
        public Ticket FindByLastName(string lastname)
        {
            return this.context.Tickets.FirstOrDefault(t => t.OccupantLastName == lastname);
        }
        public Ticket FindByPurchaseId(int purchaseid)
        {
            return this.context.Tickets.FirstOrDefault(t => t.PurchaseId == purchaseid);
        }

        public void Remove(int id)
        {
            var entity = this.context.Tickets.First(t => t.Id == id);
            this.context.Tickets.Remove(entity);
            this.context.SaveChanges();
        }

        public void Update(Ticket ticket)
        {
            this.context.Tickets.Update(ticket);
            this.context.SaveChanges();
        }
    }
}
