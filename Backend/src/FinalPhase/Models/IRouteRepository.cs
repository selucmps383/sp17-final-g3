﻿using System.Collections.Generic;
using YouFlyModels;

namespace Backend.Models
{
    public interface IRouteRepository
    {
        void Add(Route route);
        IEnumerable<Route> GetAll();
        Route Find(int id);
        Route FindByTime(int time);
        Route FindByDay(string date);
        Route FindByToId(string toid);
        Route FindByFromId(string fromid);
        void Remove(int id);
        void Update(Route route);
    }
}
