﻿using Backend.Data;
using System.Collections.Generic;
using System.Linq;
using YouFlyModels;

namespace Backend.Models
{
    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly Context context;

        public PurchaseRepository(Context context)
        {
            this.context = context;
        }

        public IEnumerable<Purchase> GetAll()
        {
            return context.Purchases.ToList();
        }
        public void Add(Purchase purchase)
        {
            this.context.Purchases.Add(purchase);
            this.context.SaveChanges();
        }

        public Purchase Find(int id)
        {
            return this.context.Purchases.FirstOrDefault(t => t.Id == id);
        }

        public Purchase FindByConfirmationNumber(int confirmationnumber)
        {
            return this.context.Purchases.FirstOrDefault(t => t.ConfirmationNumber == confirmationnumber);
        }

        public Purchase FindByUserEmail(string useremail)
        {
            return this.context.Purchases.FirstOrDefault(t => t.UserEmail == useremail);
        }
        public Purchase FindByRouteId(int routeid)
        {
            return this.context.Purchases.FirstOrDefault(t => t.RouteId == routeid);
        }
        public void Remove(int id)
        {
            var entity = this.context.Purchases.First(t => t.Id == id);
            this.context.Purchases.Remove(entity);
            this.context.SaveChanges();
        }
        public void Update(Purchase purchase)
        {
            this.context.Purchases.Update(purchase);
            this.context.SaveChanges();
        }
    }
}
