﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Backend.Data;

namespace Backend.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20170404171007_LatLon")]
    partial class LatLon
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("YouFlyModels.Airport", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<string>("Code");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Airports");
                });

            modelBuilder.Entity("YouFlyModels.Purchase", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ConfirmationNumber");

                    b.Property<string>("FlightDate");

                    b.Property<double>("Price");

                    b.Property<int>("RouteId");

                    b.Property<string>("UserEmail");

                    b.HasKey("Id");

                    b.HasIndex("ConfirmationNumber")
                        .IsUnique();

                    b.ToTable("Purchases");
                });

            modelBuilder.Entity("YouFlyModels.RemainingSpace", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Date");

                    b.Property<int>("RemainingBusinessClassSeats");

                    b.Property<int>("RemainingFirstClassSeats");

                    b.Property<int>("RouteId");

                    b.HasKey("Id");

                    b.ToTable("remainingSpaces");
                });

            modelBuilder.Entity("YouFlyModels.Route", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<int>("BusinessClassCost");

                    b.Property<int>("BusinessClassSeatCount");

                    b.Property<string>("DayOfWeek");

                    b.Property<int>("FirstClassCost");

                    b.Property<int>("FirstClassSeatCount");

                    b.Property<int>("FlightDistance");

                    b.Property<double>("FlightTime");

                    b.Property<string>("FromAirportId");

                    b.Property<int>("TimeOfDay");

                    b.Property<string>("ToAirportId");

                    b.HasKey("Id");

                    b.ToTable("Routes");
                });

            modelBuilder.Entity("YouFlyModels.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsFirstClass");

                    b.Property<string>("OccupantFirstName");

                    b.Property<string>("OccupantLastName");

                    b.Property<int>("PurchaseId");

                    b.HasKey("Id");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("YouFlyModels.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<bool>("IsAdmin");

                    b.Property<string>("Password");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.ToTable("Users");
                });
        }
    }
}
