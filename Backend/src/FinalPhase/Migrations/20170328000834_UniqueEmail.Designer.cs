﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Backend.Data;

namespace Backend.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20170328000834_UniqueEmail")]
    partial class UniqueEmail
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Backend.Models.Airport", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .IsRequired();

                    b.Property<string>("Code")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Airports");
                });

            modelBuilder.Entity("Backend.Models.Purchase", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ConfirmationNumber");

                    b.Property<string>("FlightDate")
                        .IsRequired();

                    b.Property<double>("Price");

                    b.Property<int>("RouteId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("ConfirmationNumber")
                        .IsUnique();

                    b.ToTable("Purchases");
                });

            modelBuilder.Entity("Backend.Models.RemainingSpace", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Date")
                        .IsRequired();

                    b.Property<int>("RemainingBusinessClassSeats");

                    b.Property<int>("RemainingFirstClassSeats");

                    b.Property<int>("RouteId");

                    b.HasKey("Id");

                    b.ToTable("remainingSpaces");
                });

            modelBuilder.Entity("Backend.Models.Route", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<int>("BusinessClassCost");

                    b.Property<int>("BusinessClassSeatCount");

                    b.Property<string>("DayOfWeek")
                        .IsRequired();

                    b.Property<int>("FirstClassCost");

                    b.Property<int>("FirstClassSeatCount");

                    b.Property<double>("FlightDistance");

                    b.Property<double>("FlightTime");

                    b.Property<int>("FromAirportId");

                    b.Property<double>("TimeOfDay");

                    b.Property<int>("ToAirportId");

                    b.HasKey("Id");

                    b.ToTable("Routes");
                });

            modelBuilder.Entity("Backend.Models.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsFirstClass");

                    b.Property<string>("OccupantFirstName")
                        .IsRequired();

                    b.Property<string>("OccupantLastName")
                        .IsRequired();

                    b.Property<int>("PurchaseId");

                    b.HasKey("Id");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("Backend.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<bool>("IsAdmin");

                    b.Property<string>("Password")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.ToTable("Users");
                });
        }
    }
}
