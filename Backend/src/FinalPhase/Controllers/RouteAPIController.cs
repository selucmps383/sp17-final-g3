﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using YouFlyModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    public class RouteAPIController : Controller
    {
        private readonly IRouteRepository _routeRepository;

        public RouteAPIController(IRouteRepository routeRepository)
        {
            _routeRepository = routeRepository;
        }

        [HttpGet]
        public IEnumerable<Route> GetAll()
        {
            return _routeRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetRoute")]
        [Route("byid/{id}")]
        public IActionResult GetById(int id)
        {
            var route = _routeRepository.Find(id);
            if (route == null)
            {
                return NotFound();
            }
            return new ObjectResult(route);
        }

        [HttpGet("{day}", Name = "GetRouteByDay")]
        [Route("byday/{day}")]
        public IActionResult GetByDay(string day)
        {
            var route = _routeRepository.FindByDay(day);
            if (route == null)
            {
                return NotFound();
            }
            return new ObjectResult(route);
        }

        [HttpGet("{time}", Name = "GetRouteByTime")]
        [Route("bytime/{time}")]
        public IActionResult GetByTime(int time)
        {
            var route = _routeRepository.FindByTime(time);
            if (route == null)
            {
                return NotFound();
            }
            return new ObjectResult(route);
        }

        [HttpGet("{fromid}", Name = "GetRouteByFromId")]
        [Route("byfromid/{fromid}")]
        public IActionResult GetByFromId(string fromid)
        {
            var route = _routeRepository.FindByFromId(fromid);
            if (route == null)
            {
                return NotFound();
            }
            return new ObjectResult(route);
        }

        [HttpGet("{toid}", Name = "GetRouteByToId")]
        [Route("bytoid/{toid}")]
        public IActionResult GetByToId(string toid)
        {
            var route = _routeRepository.FindByToId(toid);
            if (route == null)
            {
                return NotFound();
            }
            return new ObjectResult(route);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Route route)
        {
            if (route == null)
            {
                return BadRequest();
            }
            route.Active = true;
            _routeRepository.Add(route);

            return CreatedAtRoute("GetRoute", new { id = route.Id }, route);
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Route route)
        {
            if (route == null || route.Id != id)
            {
                return BadRequest();
            }

            var _route = _routeRepository.Find(id);
            if (_route == null)
            {
                return NotFound();
            }

            _route.Active = route.Active;
            _route.BusinessClassCost = route.BusinessClassCost;
            _route.BusinessClassSeatCount = route.BusinessClassSeatCount;
            _route.DayOfWeek = route.DayOfWeek;
            _route.FirstClassCost = route.FirstClassCost;
            _route.FirstClassSeatCount = route.FirstClassSeatCount;
            _route.FlightDistance = route.FlightDistance;
            _route.FlightTime = route.FlightTime;
            _route.FromAirportId = route.FromAirportId;
            _route.TimeOfDay = route.TimeOfDay;
            _route.ToAirportId = route.ToAirportId;

            _routeRepository.Update(_route);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var _route = _routeRepository.Find(id);
            if (_route == null)
            {
                return NotFound();
            }

            _routeRepository.Remove(id);
            return new NoContentResult();
        }
    }

}
