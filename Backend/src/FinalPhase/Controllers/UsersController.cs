using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;
using CryptoHelper;
using YouFlyModels;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Controllers
{
    public class UsersController : Controller
    {
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Index()
        {
            string myURL = "https://yfapig3.azurewebsites.net/api/userapi/";
            var client = new HttpClient();
            var response = await client.GetAsync(myURL);
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<List<User>>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return RedirectToAction("Login", "Home");
        }
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/userapi/byid/{id}");
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<User>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return RedirectToAction("Index", "Users");
        }

        public IActionResult Error()
        {
            return View();
        }
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Email,Password,IsAdmin")] User user)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty,
                    "An Error happened.");
                return View(user);
            }
            try
            {
                string myURL = "https://yfapig3.azurewebsites.net/api/userapi/";
                var client = new HttpClient();
                var response = await client.PostAsJsonAsync(myURL, user);
                if (response.ReasonPhrase == "Internal Server Error")
                {
                    return RedirectToAction("Index", "Users");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Email already exists!");
                    return View(user);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, $"Unable to create record: {ex.Message}");
            }
            return View(user);
        }
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/userapi/byid/{id}");
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<User>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return NotFound();
        }
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(include: "Id,Email,Password,IsAdmin")] User user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }
            if(user.Email == "admin@selu.edu")
            {
                user.IsAdmin = true;
            }
            user.Password = Crypto.HashPassword(user.Password);
            var client = new HttpClient();
            var response = await client.PutAsJsonAsync($"https://yfapig3.azurewebsites.net/api/userapi/{user.Id}", user);
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View(user);
        }

        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else if (id == 1)
            {
                return RedirectToAction("Index");
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/userapi/byid/{id}");
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<User>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if(id == 1)
            {
                return RedirectToAction("Index", "Users");
            }
            var client = new HttpClient();
            var response = await client.DeleteAsync($"https://yfapig3.azurewebsites.net/api/userapi/{id}");
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Users");
            }
            return NotFound();
        }


    }
}

