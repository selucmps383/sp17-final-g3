﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using YouFlyModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    public class PurchaseAPIController : Controller
    {
        private readonly IPurchaseRepository _purchaseRepository;

        public PurchaseAPIController(IPurchaseRepository purchaseRepository)
        {
            _purchaseRepository = purchaseRepository;
        }

        [HttpGet]
        public IEnumerable<Purchase> GetAll()
        {
            return _purchaseRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetPurchaseById")]
        [Route("byid/{id}")]
        public IActionResult GetById(int id)
        {
            var purchase = _purchaseRepository.Find(id);
            if (purchase == null)
            {
                return NotFound();
            }
            return new ObjectResult(purchase);
        }

        [HttpGet("{confirmationnumber}", Name = "GetPurchaseByConfirmationNumber")]
        [Route("byconfirmationnumber/{confirmationnumber}")]
        public IActionResult GetByConfirmationNumber(int confirmationnumber)
        {
            var purchase = _purchaseRepository.FindByConfirmationNumber(confirmationnumber);
            if (purchase == null)
            {
                return NotFound();
            }
            return new ObjectResult(purchase);
        }

        [HttpGet("{routeid}", Name = "GetPurchaseByRouteId")]
        [Route("byrouteid/{routeid}")]
        public IActionResult GetByRouteId(int routeid)
        {
            var purchase = _purchaseRepository.FindByRouteId(routeid);
            if (purchase == null)
            {
                return NotFound();
            }
            return new ObjectResult(purchase);
        }

        [HttpGet("{useremail}", Name = "GetPurchaseByUserEmail")]
        [Route("byuseremail/{useremail}")]
        public IActionResult GetByUserId(string useremail)
        {
            var purchase = _purchaseRepository.FindByUserEmail(useremail);
            if (purchase == null)
            {
                return NotFound();
            }
            return new ObjectResult(purchase);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Purchase purchase)
        {
            if (purchase == null)
            {
                return BadRequest();
            }

            _purchaseRepository.Add(purchase);

            return CreatedAtRoute("GetPurchase", new { id = purchase.Id }, purchase);
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Purchase purchase)
        {
            if (purchase == null || purchase.Id != id)
            {
                return BadRequest();
            }

            var _purchase = _purchaseRepository.Find(id);
            if (_purchase == null)
            {
                return NotFound();
            }

            _purchase.ConfirmationNumber = purchase.ConfirmationNumber;
            _purchase.FlightDate = purchase.FlightDate;
            _purchase.Price = purchase.Price;
            _purchase.RouteId = purchase.RouteId;
            _purchase.UserEmail = purchase.UserEmail;

            _purchaseRepository.Update(_purchase);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var _purchase = _purchaseRepository.Find(id);
            if (_purchase == null)
            {
                return NotFound();
            }

            _purchaseRepository.Remove(id);
            return new NoContentResult();
        }
    }

}
