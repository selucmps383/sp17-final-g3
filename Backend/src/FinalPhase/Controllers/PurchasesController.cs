using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;
using CryptoHelper;
using YouFlyModels;
using Microsoft.AspNetCore.Authorization;

namespace Backend.Controllers
{
    public class PurchasesController : Controller
    {
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Index()
        {
            string myURL = "https://yfapig3.azurewebsites.net/api/purchaseapi/";
            var client = new HttpClient();
            var response = await client.GetAsync(myURL);
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<List<Purchase>>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult Error()
        {
            return View();
        }

    }
}

