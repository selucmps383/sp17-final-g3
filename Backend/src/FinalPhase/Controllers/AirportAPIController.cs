﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using YouFlyModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    public class AirportAPIController : Controller
    {
        private readonly IAirportRepository _airportRepository;

        public AirportAPIController(IAirportRepository airportRepository)
        {
            _airportRepository = airportRepository;
        }

        [HttpGet]
        public IEnumerable<Airport> GetAll()
        {
            return _airportRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetAirportById")]
        [Route("byid/{id}")]
        public IActionResult GetById(int id)
        {
            var airport = _airportRepository.Find(id);
            if (airport == null)
            {
                return NotFound();
            }
            return new ObjectResult(airport);
        }

        [HttpGet("{city}", Name = "GetAirportByCity")]
        [Route("bycity/{city}")]
        public IActionResult GetByCity(string city)
        {
            var airport = _airportRepository.FindByCity(city);
            if (airport == null)
            {
                return NotFound();
            }
            return new ObjectResult(airport);
        }

        [HttpGet("{name}", Name = "GetAirportByName")]
        [Route("byname/{name}")]
        public IActionResult GetByName(string name)
        {
            var airport = _airportRepository.FindByName(name);
            if (airport == null)
            {
                return NotFound();
            }
            return new ObjectResult(airport);
        }

        [HttpGet("{code}", Name = "GetAirportByCode")]
        [Route("bycode/{code}")]
        public IActionResult GetByCode(string code)
        {
            var airport = _airportRepository.FindByCode(code);
            if (airport == null)
            {
                return NotFound();
            }
            return new ObjectResult(airport);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Airport airport)
        {
            if (airport == null)
            {
                return BadRequest();
            }

            _airportRepository.Add(airport);

            return CreatedAtRoute("GetAirport", new { id = airport.Id }, airport);
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Airport airport)
        {
            if (airport == null || airport.Id != id)
            {
                return BadRequest();
            }

            var _airport = _airportRepository.Find(id);
            if (_airport == null)
            {
                return NotFound();
            }
            
            _airport.Name = airport.Name;
            _airport.Code = airport.Code;
            _airport.City = airport.City;

            _airportRepository.Update(_airport);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var _airport = _airportRepository.Find(id);
            if (_airport == null)
            {
                return NotFound();
            }

            _airportRepository.Remove(id);
            return new NoContentResult();
        }
    }
    
}
