using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using System.Net.Http;
using Newtonsoft.Json;
using YouFlyModels;
using Microsoft.AspNetCore.Authorization;
using Backend.Data;

namespace Backend.Controllers
{
    public class RoutesController : Controller
    {
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Index()
        {
            string myURL = "https://yfapig3.azurewebsites.net/api/routeapi/";
            var client = new HttpClient();
            var response = await client.GetAsync(myURL);
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<List<Route>>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            ModelState.AddModelError(string.Empty,
                    "Something went wrong.");
            return RedirectToAction("Login", "Home");
        }
        [Authorize(Roles = "ADMIN")]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/userapi/byid/{id}");
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<Route>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            ModelState.AddModelError(string.Empty,
                    "A bad happened.");
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View();
        }
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(
            "FromAirportId,ToAirportId,FirstClassSeatCount,BusinessClassSeatCount,FirstClassCost,BusinessClassCost,DayOfWeek,TimeOfDay,FlightTime,FlightDistance,Active"
            )] Route route)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty,
                    "An Error happened.");
                return View(route);
            }
            try
            {
                string myURL = "https://yfapig3.azurewebsites.net/api/routeapi/";
                var client = new HttpClient();

                string fromURL = "https://yfapig3.azurewebsites.net/api/airportapi/bycode/"+route.FromAirportId;
                string toURL = "https://yfapig3.azurewebsites.net/api/airportapi/bycode/" + route.ToAirportId;
                var fromAirport = await client.GetAsync(fromURL);
                var toAirport = await client.GetAsync(toURL);

                var airportX = JsonConvert.DeserializeObject<Airport>(await fromAirport.Content.ReadAsStringAsync());
                var airportY= JsonConvert.DeserializeObject<Airport>(await toAirport.Content.ReadAsStringAsync());
                route.FlightDistance = (int) findDistance(airportX.Latitude, airportY.Latitude, airportX.Longitude, airportY.Longitude);

                route.BusinessClassCost = (route.FlightDistance / 100) + 50;
                route.FirstClassCost = route.BusinessClassCost * 2;
                

                //assuming 500 knots (average speed of most large airplanes)
                route.FlightTime = route.FlightDistance / 575.39;
                route.FlightTime = Math.Truncate(100 * route.FlightTime) / 100;


                var response = await client.PostAsJsonAsync(myURL, route);
               
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Create");
                }
                //else
                //{
                //    ModelState.AddModelError(string.Empty, "Email already exists!");
                //    return View(user);
                //}
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, $"Unable to create record: {ex.Message}");
            }
            return View(route);
        }

        private double findDistance(double lat1, double lat2, double lon1, double lon2)
        {
            double theta1 = lat1 - lat2;
            double theta2 = lon1 - lon2;
            double convert = Math.PI / 180;
            lat1 = lat1 * convert;
            lat2 = lat2 * convert;
            lon1 = lon1 * convert;
            lon2 = lon2 * convert;
            theta1 = theta1 * convert;
            theta2 = theta2 * convert;

            var a = ((Math.Sin(theta1 / 2)) * (Math.Sin(theta1 / 2))) + Math.Cos(lat1) * Math.Cos(lat2) * (Math.Sin(theta2 / 2) * Math.Sin(theta2 / 2));
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = 6371 * c;

            d = d * .621371;
            
            /*double dist = (Math.Sin(lat1) * (Math.Sin(lat2)) + (Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(theta)));
            dist = Math.Acos(dist);
            dist = dist * (180 / Math.PI);
            */
            return d;
        }

        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/routeapi/byid/{id}");
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<Route>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return NotFound();
        }
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(include: 
            "Id,FromAirportId,ToAirportId,FirstClassSeatCount,BusinessClassSeatCount,FirstClassCost,BusinessClassCost,DayOfWeek,TimeOfDay,FlightTime,FlightDistance,Active"
            )] Route route)
        {
            if (!ModelState.IsValid)
            {
                return View(route);
            }
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var client = new HttpClient();

                string fromURL = "https://yfapig3.azurewebsites.net/api/airportapi/bycode/" + route.FromAirportId;
                string toURL = "https://yfapig3.azurewebsites.net/api/airportapi/bycode/" + route.ToAirportId;
                var fromAirport = await client.GetAsync(fromURL);
                var toAirport = await client.GetAsync(toURL);

                var airportX = JsonConvert.DeserializeObject<Airport>(await fromAirport.Content.ReadAsStringAsync());
                var airportY = JsonConvert.DeserializeObject<Airport>(await toAirport.Content.ReadAsStringAsync());
                route.FlightDistance = (int)findDistance(airportX.Latitude, airportY.Latitude, airportX.Longitude, airportY.Longitude);

                route.BusinessClassCost = (route.FlightDistance / 100) + 50;
                route.FirstClassCost = route.BusinessClassCost * 2;


                //assuming 500 knots (average speed of most large airplanes)
                route.FlightTime = route.FlightDistance / 575.39;
                route.FlightTime = Math.Truncate(100 * route.FlightTime) / 100;

                response = await client.PutAsJsonAsync($"https://yfapig3.azurewebsites.net/api/routeapi/{route.Id}", route);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, $"Unable to create record: {ex.Message}");
            }
            
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View(route);
        }

        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var client = new HttpClient();
            var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/routeapi/byid/{id}");
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<Route>(
                    await response.Content.ReadAsStringAsync());
                return View(items);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var client = new HttpClient();
            var response = await client.DeleteAsync($"https://yfapig3.azurewebsites.net/api/routeapi/{id}");
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index","Routes");
            }
            return NotFound();
        }


    }
}

