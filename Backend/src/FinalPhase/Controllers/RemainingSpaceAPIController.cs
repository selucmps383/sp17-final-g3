﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using YouFlyModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    public class RemainingSpaceAPIController : Controller
    {
        private readonly IRemainingSpaceRepository _remainingSpaceRepository;

        public RemainingSpaceAPIController(IRemainingSpaceRepository remainingSpaceRepository)
        {
            _remainingSpaceRepository = remainingSpaceRepository;
        }

        [HttpGet]
        public IEnumerable<RemainingSpace> GetAll()
        {
            return _remainingSpaceRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetRemainingSpaceById")]
        [Route("byid/{id}")]
        public IActionResult GetById(int id)
        {
            var remainingSpace = _remainingSpaceRepository.Find(id);
            if (remainingSpace == null)
            {
                return NotFound();
            }
            return new ObjectResult(remainingSpace);
        }

        [HttpGet("{routeid}", Name = "GetRemainingSpaceByRouteId")]
        [Route("byrouteid/{routeid}")]
        public IActionResult GetByRouteId(int routeid)
        {
            var remainingSpace = _remainingSpaceRepository.FindByRouteId(routeid);
            if (remainingSpace == null)
            {
                return NotFound();
            }
            return new ObjectResult(remainingSpace);
        }

        [HttpPost]
        public IActionResult Create([FromBody] RemainingSpace remainingSpace)
        {
            if (remainingSpace == null)
            {
                return BadRequest();
            }

            _remainingSpaceRepository.Add(remainingSpace);

            return CreatedAtRoute("GetRemainingSpace", new { id = remainingSpace.Id }, remainingSpace);
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] RemainingSpace remainingSpace)
        {
            if (remainingSpace == null || remainingSpace.Id != id)
            {
                return BadRequest();
            }

            var _remainingSpace = _remainingSpaceRepository.Find(id);
            if (_remainingSpace == null)
            {
                return NotFound();
            }

            _remainingSpace.Date = remainingSpace.Date;
            _remainingSpace.RemainingBusinessClassSeats = remainingSpace.RemainingBusinessClassSeats;
            _remainingSpace.RemainingFirstClassSeats = remainingSpace.RemainingFirstClassSeats;
            _remainingSpace.RouteId = remainingSpace.RouteId;

            _remainingSpaceRepository.Update(_remainingSpace);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var _remainingSpace = _remainingSpaceRepository.Find(id);
            if (_remainingSpace == null)
            {
                return NotFound();
            }

            _remainingSpaceRepository.Remove(id);
            return new NoContentResult();
        }
    }

}
