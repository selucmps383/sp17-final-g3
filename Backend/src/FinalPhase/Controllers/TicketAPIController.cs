﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using YouFlyModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    public class TicketAPIController : Controller
    {
        private readonly ITicketRepository _ticketRepository;

        public TicketAPIController(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        [HttpGet]
        public IEnumerable<Ticket> GetAll()
        {
            return _ticketRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetTicketById")]
        [Route("byid/{id}")]
        public IActionResult GetById(int id)
        {
            var ticket = _ticketRepository.Find(id);
            if (ticket == null)
            {
                return NotFound();
            }
            return new ObjectResult(ticket);
        }

        [HttpGet("{firstname}", Name = "GetTicketByFirstName")]
        [Route("byfirstname/{firstname}")]
        public IActionResult GetByFirstName(string firstname)
        {
            var ticket = _ticketRepository.FindByFirstName(firstname);
            if (ticket == null)
            {
                return NotFound();
            }
            return new ObjectResult(ticket);
        }

        [HttpGet("{lastname}", Name = "GetTicketByLastName")]
        [Route("bylastname/{lastname}")]
        public IActionResult GetByLastName(string lastname)
        {
            var ticket = _ticketRepository.FindByFirstName(lastname);
            if (ticket == null)
            {
                return NotFound();
            }
            return new ObjectResult(ticket);
        }

        [HttpGet("{purchaseid}", Name = "GetTicketByPurchaseId")]
        [Route("bypurchaseid/{purchaseid}")]
        public IActionResult GetByPurchaseId(int purchaseid)
        {
            var ticket = _ticketRepository.FindByPurchaseId(purchaseid);
            if (ticket == null)
            {
                return NotFound();
            }
            return new ObjectResult(ticket);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Ticket ticket)
        {
            if (ticket == null)
            {
                return BadRequest();
            }

            _ticketRepository.Add(ticket);

            return CreatedAtRoute("GetTicket", new { id = ticket.Id }, ticket);
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Ticket ticket)
        {
            if (ticket == null || ticket.Id != id)
            {
                return BadRequest();
            }

            var _ticket = _ticketRepository.Find(id);
            if (_ticket == null)
            {
                return NotFound();
            }

            _ticket.IsFirstClass = ticket.IsFirstClass;
            _ticket.OccupantFirstName = ticket.OccupantFirstName;
            _ticket.OccupantLastName = ticket.OccupantLastName;
            _ticket.PurchaseId = ticket.PurchaseId;

            _ticketRepository.Update(_ticket);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var _ticket = _ticketRepository.Find(id);
            if (_ticket == null)
            {
                return NotFound();
            }

            _ticketRepository.Remove(id);
            return new NoContentResult();
        }
    }

}
