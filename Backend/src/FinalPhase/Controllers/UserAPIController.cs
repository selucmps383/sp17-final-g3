﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;
using CryptoHelper;
using YouFlyModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    public class UserAPIController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UserAPIController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetUserById")]
        [Route("byid/{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userRepository.Find(id);
            if (user == null)
            {
                return NotFound();
            }
            return new ObjectResult(user);
        }

        [HttpGet("{email}", Name = "GetUserByEmail")]
        [Route("byemail/{email}")]
        public IActionResult GetByEmail(string email)
        {
            var user = _userRepository.FindByEmail(email);
            if (user == null)
            {
                return NotFound();
            }
            return new ObjectResult(user);
        }

        [HttpPost]
        public IActionResult Create([FromBody] User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            if (!((_userRepository.FindByEmail(user.Email)) == null))
            {
                return BadRequest();
            }
            else
            {
                user.Password = Crypto.HashPassword(user.Password);
                _userRepository.Add(user);

                return CreatedAtRoute("GetUser", new { id = user.Id }, user);
            }
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] User user)
        {
            if (user == null || user.Id != id)
            {
                return BadRequest();
            }

            var _user = _userRepository.Find(id);
            if (_user == null)
            {
                return NotFound();
            }
            if (!((_userRepository.FindByEmail(user.Email)) == null) && (!(_user.Email == user.Email)))
            {
                return BadRequest();
            }
            else
            {
                _user.IsAdmin = user.IsAdmin;
                _user.Email = user.Email;
                _user.Password = user.Password;

                _userRepository.Update(_user);
                return new NoContentResult();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var _user = _userRepository.Find(id);
            if (_user == null)
            {
                return NotFound();
            }

            _userRepository.Remove(id);
            return new NoContentResult();
        }
    }

}
