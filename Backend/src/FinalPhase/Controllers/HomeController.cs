﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using CryptoHelper;
using System.Security.Claims;
using Backend.Models;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YouFlyModels;
using Backend.Data;
using System.Net.Http;

namespace Backend.Controllers
{
    public class HomeController : Controller
    {
        private readonly Context _context;
        private bool adminStatus;
        private string passChecker;
        public HomeController(Context context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [Authorize(Roles = "ADMIN")]
        public IActionResult Landing()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("MyCookieMiddlewareInstance");
            return RedirectToAction("Login", "Home");
        }

        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response => {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }

        [HttpPost]
        public async Task<IActionResult> LoginAccount(User model)
        {
            /*System.Diagnostics.Debug.WriteLine("1");
            string myURL = "https://yfapig3.azurewebsites.net/api/userapi/";
            if (ModelState.IsValid)
            {
                System.Diagnostics.Debug.WriteLine("1.5");
                RestClient myClient = new RestClient(myURL + "byemail/" + model.Email);
                RestRequest myRequest = new RestRequest(Method.GET);
                var myResponse = new RestResponse();
                Task.Run(async () =>
                {
                    myResponse = await GetResponseContentAsync(myClient, myRequest) as RestResponse;
                }).Wait();
                System.Diagnostics.Debug.WriteLine("2");
                var jsonResponse = JsonConvert.DeserializeObject<JObject>(myResponse.Content);
                //System.Diagnostics.Debug.WriteLine(jsonResponse);

                if (myResponse.ErrorException == null)
                {

                    if (Crypto.VerifyHashedPassword(JsonConvert.DeserializeObject<User>(myResponse.Content).Password, model.Password))
                    {
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.NameIdentifier, model.Email)
                        };
                        var identity = new ClaimsIdentity(claims, model.Email);
                        if (JsonConvert.DeserializeObject<User>(myResponse.Content).IsAdmin == true)
                        {
                            System.Diagnostics.Debug.WriteLine("if hit");
                            identity.AddClaim(new Claim(ClaimTypes.Role, "ADMIN"));
                        }
                        else
                        {
                            ModelState.AddModelError("", "Sorry, Admins only!");
                        }

                        var claimsPrincipal = new ClaimsPrincipal(identity);
                        await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", claimsPrincipal);
                        System.Diagnostics.Debug.WriteLine("3");
                        return RedirectToAction("Landing", "Home");
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("4");
                    ModelState.AddModelError("", "Invalid login attempt");
                    return RedirectToAction("Login", "Home");
                }
            }
            System.Diagnostics.Debug.WriteLine("5");
            ModelState.AddModelError("", "Invalid login attempt");
            return RedirectToAction("Login", "Home");
            */
            if (ModelState.IsValid)
            {
                var client = new HttpClient();
                var response = await client.GetAsync($"https://yfapig3.azurewebsites.net/api/userapi/byemail/{model.Email}");
                if (response.IsSuccessStatusCode)
                {
                    var items = JsonConvert.DeserializeObject<User>(
                        await response.Content.ReadAsStringAsync());
                    adminStatus = items.IsAdmin;
                    passChecker = items.Password;
                }
                if (adminStatus == true)
                {
                    if (Crypto.VerifyHashedPassword(passChecker, model.Password))
                    {
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.NameIdentifier, model.Email)
                        };
                        var identity = new ClaimsIdentity(claims, model.Email);
                        identity.AddClaim(new Claim(ClaimTypes.Role, "ADMIN"));

                        var claimsPrincipal = new ClaimsPrincipal(identity);
                        await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", claimsPrincipal);
                        return RedirectToAction("Landing", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Password Incorrect!");
                        return RedirectToAction("Login", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Admins Only, Check Your Login Credentials!");
                    return RedirectToAction("Login", "Home");
                }
            }
            ModelState.AddModelError("", "Invalid login attempt");
            return RedirectToAction("Login", "Home");
        }
    }
}
