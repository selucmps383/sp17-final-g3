﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyModels
{
    public class Ticket
    {
        public int Id { get; set; }
        
        public int PurchaseId { get; set; }
    
        public bool IsFirstClass { get; set; }
        
        public string OccupantFirstName { get; set; }
        
        public string OccupantLastName { get; set; }
    }
}
