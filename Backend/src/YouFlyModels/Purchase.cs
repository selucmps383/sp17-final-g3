﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyModels
{
    public class Purchase
    {
        public int Id { get; set; }
        
        public double Price { get; set; }
        
        public int ConfirmationNumber { get; set; }
        
        public string FlightDate { get; set; }
        
        public int RouteId { get; set; }
        
        public string UserEmail { get; set; }
    }
}
