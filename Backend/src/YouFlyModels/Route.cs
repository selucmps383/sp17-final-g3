﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyModels
{
    public class Route
    {
        public int Id { get; set; }
        
        public string FromAirportId { get; set; }
        
        public string ToAirportId { get; set; }
        
        public int FirstClassSeatCount { get; set; }
        
        public int BusinessClassSeatCount { get; set; }
        
        public int FirstClassCost { get; set; }
        
        public int BusinessClassCost { get; set; }
        
        public string DayOfWeek { get; set; }
        
        public int TimeOfDay { get; set; }
        
        public double FlightTime { get; set; }
        
        public int FlightDistance { get; set; }
        
        public bool Active { get; set; }
    }
}
