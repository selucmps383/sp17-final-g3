﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyModels
{
    public class RemainingSpace
    {
        public int Id { get; set; }
        
        public int RouteId { get; set; }
        
        public string Date { get; set; }
        
        public int RemainingFirstClassSeats { get; set; }
        
        public int RemainingBusinessClassSeats { get; set; }
    }
}
