﻿using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using YouFlyModels;
using YouFlyApp.Activities;
using YouFlyApp.Classes;

namespace YouFlyApp
{
    public class PurchaseHistoryAdapter : BaseAdapter<Purchase>
    {
        List<Purchase> purchases;
        Context adapterContext;

        public PurchaseHistoryAdapter(Context adapterContext, List<Purchase> purchases)
        {
            this.purchases = purchases;
            this.adapterContext = adapterContext;
        }
        public override int Count
        {
            get
            {
                return purchases.Count();
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Purchase this[int position]
        {
            get { return purchases[position]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            if (row == null)
            {
                row = LayoutInflater.From(adapterContext).Inflate(Resource.Layout.Purchases, null, false);
            }

            LinearLayout rowLayout = row.FindViewById<LinearLayout>(Resource.Id.listLayout);
            TextView purchaseDetails = row.FindViewById<TextView>(Resource.Id.noPurchaseText);

            var flightDate = purchases[position].FlightDate;
            var confirmationNum = purchases[position].ConfirmationNumber;

            rowLayout.Click += delegate
            {
                sendTicketInfo(adapterContext, purchases[position]);
            };
            if(confirmationNum > 0)
            {
                purchaseDetails.Text = "Flight Date: " + flightDate + "\nYF#" + confirmationNum;
            }
            else
            {
                purchaseDetails.Text = "No Purchases Made!";
            }
            

            return row;
        }
        public static void sendTicketInfo(Context adapterContext, Purchase purchase)
        {
            if(purchase.ConfirmationNumber > 0)
            {
                YFLoader.loadMessage(adapterContext);
                Intent intent = new Intent(adapterContext, typeof(PurchaseDetailActivity));
                intent.PutExtra("purchase", Newtonsoft.Json.JsonConvert.SerializeObject(purchase));
                adapterContext.StartActivity(intent);
            }
        }
    }
}