﻿using System;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using System.Collections.Generic;
using YouFlyModels;
using YouFlyApp.Fragments;
using RestSharp;

namespace YouFly.Adapters
{
    class TicketListAdapter : FragmentPagerAdapter
    {
        public List<Ticket> ticketList;
        public Purchase purchase;
        public Route route;

        public TicketListAdapter(FragmentManager fm, List<Ticket> ticketList) : base(fm)
        {
            this.ticketList = ticketList;
        }

        private Purchase getPurchase(int purchaseId)
        {
            string url = "/api/purchaseapi/byconfirmationnumber/" + purchaseId;
            var client = new RestClient("https://yfapig3.azurewebsites.net");
            RestRequest request = new RestRequest(url, Method.GET);
            IRestResponse response = client.Execute<Purchase>(request);
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Purchase>(response.Content);

            return data;
        }
        private Route getRoute(int routeId)
        {
            string url = "/api/routeapi/byid/" + routeId;
            var client = new RestClient("https://yfapig3.azurewebsites.net");
            RestRequest request = new RestRequest(url, Method.GET);
            IRestResponse response = client.Execute<Route>(request);
            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Route>(response.Content);

            return data;
        }

        public override int Count
        {
            get { return ticketList.Count; }
        }

        public override Fragment GetItem(int position)
        {
            purchase = this.getPurchase(ticketList[0].PurchaseId);
            route = getRoute(purchase.RouteId);
            return (Fragment)TicketFragment.newInstance(ticketList[position], purchase, route);
        }
    }
}