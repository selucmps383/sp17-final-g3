using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using YouFlyModels;
using RestSharp;
using Newtonsoft.Json;
using YouFlyApp.Activities;

namespace YouFlyApp
{
    [Activity(Label = "Purchase List")]
    public class PurchasesActivity : ListActivity
    {
        private string userEmail;
        private int purchaseAmount;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //SetContentView(Resource.Layout.Purchases);
            
            userEmail = Intent.GetStringExtra("userEmail");
            string url = "/api/purchaseapi/byuseremail/" + userEmail;

            var purchaseList = getPurchaseConNumbers(url);
            if (purchaseAmount > 0)
            {
                ListAdapter = new PurchaseHistoryAdapter(this, purchaseList);
            }
            else
            {
                var nullList = new List<Purchase>();
                var nullPurchase = new Purchase();
                nullPurchase.ConfirmationNumber = 0;
                nullPurchase.FlightDate = "No purchases made!";
                nullList.Add(nullPurchase);
                ListAdapter = new PurchaseHistoryAdapter(this, nullList);
            }
        }
        
        public override void OnBackPressed()
        {
            StartActivity(typeof(LogoutActivity));
        }
        public List<Purchase> getPurchaseConNumbers(string url)
        {
            List<Purchase> conNumbers = new List<Purchase>();
            var client = new RestClient("https://yfapig3.azurewebsites.net");
            RestRequest request = new RestRequest(url, Method.GET);

            request.AddHeader("Content-type", "application/json");

            var response = client.Execute<Purchase>(request);
            var data = JsonConvert.DeserializeObject<IEnumerable<Purchase>>(response.Content);
            
            foreach (Purchase t in data)
            {
                conNumbers.Add(t);
                purchaseAmount++;
            }
            return conNumbers;
        }
    }
}