﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using YouFlyApp.Activities;
using CryptoHelper;
using RestSharp;
using YouFlyModels;
using Newtonsoft.Json;
using YouFlyApp.Classes;
using Android.Graphics;

namespace YouFlyApp
{
    [Activity(Label = "YouFly", MainLauncher = true, Icon = "@drawable/YouFlyIcon")]
    public class MainActivity : Activity
    {
        private Button btnLogin, btnRegister, btnTicket;
        private TextView txtEmail, txtPassword;
        private string userEmail, password;
        private ProgressDialog progress;
        private IRestResponse<User> response;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            btnRegister = FindViewById<Button>(Resource.Id.btnRegister);

            txtEmail = FindViewById<EditText>(Resource.Id.email);
            txtPassword = FindViewById<EditText>(Resource.Id.password);

            btnLogin.Click += btnLoginClick;
            btnRegister.Click += btnRegisterClick;

            txtEmail.Click += txtViewClick;
            txtPassword.Click += txtViewClick;
        }

        public override void OnBackPressed()
        {

        }
        void txtViewClick(object sender, EventArgs e)
        {
            txtEmail.SetTextColor(Color.Rgb(39, 170, 225));
            txtPassword.SetTextColor(Color.Rgb(39, 170, 225));
        }
        void btnLoginClick(object sender, EventArgs e)
        {
            userEmail = txtEmail.Text;
            password = txtPassword.Text;
            try
            {
                string url = "/api/userapi/byemail/" + userEmail;
                var client = new RestClient("https://yfapig3.azurewebsites.net");
                RestRequest request = new RestRequest(url, Method.GET);
                response = client.Execute<User>(request);

                var data = JsonConvert.DeserializeObject<User>(response.Content);
                if (Crypto.VerifyHashedPassword(data.Password, password))
                {
                    YFLoader.loadMessage(this);
                    Intent myIntent = new Intent(this, typeof(PurchasesActivity));
                    myIntent.PutExtra("userEmail", userEmail);

                    StartActivity(myIntent);
                }
                else
                {
                    FindViewById<EditText>(Resource.Id.email).CurrentTextColor.Equals(Color.Red);
                    Toast.MakeText(this, "Invalid Login", ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                FindViewById<EditText>(Resource.Id.email).SetTextColor(Color.Red);
                Toast.MakeText(this, "Invalid Login", ToastLength.Long).Show();
            }

        }
        void btnRegisterClick(object sender, EventArgs e)
        {
            YFLoader.loadMessage(this);
            Intent registerIntent = new Intent(this, typeof(RegisterActivity));
            StartActivity(registerIntent);
        }
    }
}

