using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YouFlyApp.Activities
{
    [Activity(Label = "Logout")]

    public class LogoutActivity : Activity
    {
        private Button logoutButton;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Logout);
            logoutButton = FindViewById<Button>(Resource.Id.btnLogout);
            logoutButton.Click += logoutButtonClick;
        }

        void logoutButtonClick(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Logging out...", ToastLength.Long).Show();
            StartActivity(typeof(MainActivity));
        }
    }
}