﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using YouFlyModels;
using RestSharp;
using Newtonsoft.Json;
using YouFlyApp.Activities;
using YouFlyApp.Fragments;
using YouFlyApp.Classes;

namespace YouFlyApp.Activities
{
    [Activity(Label = "Purchase Details")]
    public class PurchaseDetailActivity : Activity
    {
        private TextView txtConNum, txtDate, txtTime, txtPrice, txtPassengers;
        private Button btnTickets;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.PurchaseDetail);

            txtConNum = FindViewById<TextView>(Resource.Id.txtConNum);
            txtDate = FindViewById<TextView>(Resource.Id.txtDate);
            txtTime = FindViewById<TextView>(Resource.Id.txtTime);
            txtPrice = FindViewById<TextView>(Resource.Id.txtPrice);
            txtPassengers = FindViewById<TextView>(Resource.Id.txtPassengers);

            btnTickets = FindViewById<Button>(Resource.Id.btnTickets);

            Purchase currentPurchase = JsonConvert.DeserializeObject<Purchase>(Intent.GetStringExtra("purchase"));
            txtConNum.Text = "Confirmation Number: YF#" + currentPurchase.ConfirmationNumber.ToString();
            txtDate.Text = "Flight Date: " + currentPurchase.FlightDate;
            txtPrice.Text = "Price: $" + currentPurchase.Price + ".00";
            txtTime.Text = "Takeoff Time: " + getRouteTime(currentPurchase.RouteId);

            string url = "/api/ticketapi/bypurchaseid/" + currentPurchase.ConfirmationNumber.ToString();
            List<string> passengerList = getPassengers(url);
            ListView listView = (ListView)FindViewById(Resource.Id.ListView);
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, passengerList);
            listView.Adapter = adapter;

            btnTickets.Click += delegate
            {
                btnTicketsClick(currentPurchase);
            };
        }

        void btnTicketsClick(Purchase currentPurchase)
        {
            YFLoader.loadMessage(this);
            Intent ticketsIntent = new Intent(this, typeof(TicketsFragmentActivity));
            ticketsIntent.PutExtra("purchase", JsonConvert.SerializeObject(currentPurchase));
            this.StartActivity(ticketsIntent);
        }

        void btnRegisterClick(object sender, EventArgs e)
        {
            Intent registerIntent = new Intent(this, typeof(RegisterActivity));
            StartActivity(registerIntent);
        }

        public List<string> getPassengers(string url)
        {
            List<Ticket> passengers = new List<Ticket>();
            var client = new RestClient("https://yfapig3.azurewebsites.net");
            RestRequest request = new RestRequest(url, Method.GET);

            request.AddHeader("Content-type", "application/json");

            var response = client.Execute<Purchase>(request);
            var data = JsonConvert.DeserializeObject<IEnumerable<Ticket>>(response.Content);

            List<string> fullNames = new List<string>();
            foreach (Ticket t in data)
            {
                fullNames.Add(t.OccupantFirstName + " " + t.OccupantLastName);
            }
            return fullNames;

        }

        string getRouteTime(int routeId)
        {
            string url = "/api/routeapi/byid/" + routeId;
            var client = new RestClient("https://yfapig3.azurewebsites.net");
            RestRequest request = new RestRequest(url, Method.GET);

            request.AddHeader("Content-type", "application/json");

            var response = client.Execute<Route>(request);
            var data = JsonConvert.DeserializeObject<Route>(response.Content);

            string timeString = data.TimeOfDay.ToString() + ":00 AM";
            if (data.TimeOfDay > 12 && data.TimeOfDay < 24)
            {
                var temp = data.TimeOfDay - 12;
                timeString = temp.ToString() + ":00 PM";
            }
            else if (data.TimeOfDay == 24)
            {
                timeString = "12:00 AM";
            }
            return timeString;

        }
    }
}