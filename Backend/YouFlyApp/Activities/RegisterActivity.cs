﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using YouFlyModels;

using RestSharp;
using YouFlyApp.Classes;

namespace YouFlyApp.Activities
{
    [Activity(Label = "RegisterActivity")]
    public class RegisterActivity : Activity
    {
        private string email;
        private string password;
        private string confirmPassword;

        private Button btnSubmit;
        private RestClient client = new RestClient("https://yfapig3.azurewebsites.net");
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Register);

            btnSubmit = FindViewById<Button>(Resource.Id.submit);

            btnSubmit.Click += btnSubmitClick;
        }

        void btnSubmitClick(object sender, EventArgs e)
        {
            email = FindViewById<EditText>(Resource.Id.registerEmail).Text;
            password = FindViewById<EditText>(Resource.Id.registerPassword).Text;
            confirmPassword = FindViewById<EditText>(Resource.Id.registerConfirmPassword).Text;

            if(!(Android.Util.Patterns.EmailAddress.Matcher(email).Matches()))
            {
                Toast.MakeText(this, "Not a valid email!", ToastLength.Long).Show();
            }
            else if (password == confirmPassword)
            {
                var request = new RestRequest("/api/userapi/", Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.AddBody(new User
                {
                    Email = email,
                    Password = password
                });
                client.Execute(request);

                Intent mainActivity = new Intent(this, typeof(MainActivity));
                YFLoader.loadMessage(this);
                StartActivity(mainActivity);

            }
            else
            {
                Toast.MakeText(this, "Passwords do not match!", ToastLength.Long).Show();
            }
        }
    }
}