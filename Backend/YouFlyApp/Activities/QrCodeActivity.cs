using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YouFlyApp.Activities
{
    [Activity(Label = "Boarding Pass")]
    public class QrActivity : Activity
    {
        private ImageView imageBox;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.QR);

            imageBox = FindViewById<ImageView>(Resource.Id.imageBox);
            var barcodeWriter = new ZXing.Mobile.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions
                {
                    Width = 600,
                    Height = 600
                }
            };
            var code = barcodeWriter.Write("WELLLL WHADDYA KNOW");
            imageBox.SetImageBitmap(code);
        }
    }
}