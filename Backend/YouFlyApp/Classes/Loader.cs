﻿using Android.App;
using Android.Content;

namespace YouFlyApp.Classes
{
   public class YFLoader
    {
        public static void loadMessage(Context context)
        {
            ProgressDialog progress;
            progress = new ProgressDialog(context);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Loading... Please wait...");
            progress.SetCancelable(true);
            progress.Show();
        }
    }
}