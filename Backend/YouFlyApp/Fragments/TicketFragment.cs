﻿using System;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using YouFlyApp;
using YouFlyModels;
using RestSharp;

namespace YouFlyApp.Fragments
{
    public class TicketFragment : Fragment
    {
        private static string firstName = "firstName";
        private static string lastName = "lastName";
        private static string date = "date";
        private static string time = "time";
        private static string fromId = "fromId";
        private static string toId = "toId";
        private static string seatType = "seatType";
        private static string conNumber = "purchaseId";
        private IRestResponse<Purchase> response;
        public TicketFragment() { }

        public static TicketFragment newInstance(Ticket ticket, Purchase purchase, Route route)
        {
            TicketFragment fragment = new TicketFragment();

            Bundle args = new Bundle();

            args.PutString(firstName, ticket.OccupantFirstName);
            args.PutString(lastName, ticket.OccupantLastName);

            args.PutString(date, purchase.FlightDate);
            args.PutInt(time, route.TimeOfDay);

            args.PutString(fromId, route.FromAirportId);
            args.PutString(toId, route.ToAirportId);

            args.PutString(seatType, ticket.IsFirstClass.ToString());
            args.PutInt(conNumber, ticket.PurchaseId);
            fragment.Arguments = args;

            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            string fName = Arguments.GetString(firstName, "");
            string lName = Arguments.GetString(lastName, "");
            string sType = Arguments.GetString(seatType, "");
            string fDate = Arguments.GetString(date, "");
            string fId = Arguments.GetString(fromId, "");
            string tId = Arguments.GetString(toId, "");
            int fTime = Arguments.GetInt(time, 1);
            int con = Arguments.GetInt(conNumber, 1);
            Boolean isFirstClass = Convert.ToBoolean(sType);
            String seatTypeName;

            View view = inflater.Inflate(Resource.Layout.TicketFragment, container, false);

            TextView txtName = (TextView)view.FindViewById(Resource.Id.txtName);
            TextView txtDate = (TextView)view.FindViewById(Resource.Id.txtDate);
            TextView txtDepartureTime = (TextView)view.FindViewById(Resource.Id.txtDepartureTime);
            TextView txtSeatType = (TextView)view.FindViewById(Resource.Id.txtSeatType);
            TextView txtRoute = (TextView)view.FindViewById(Resource.Id.txtRoute);
            TextView txtConNumber = (TextView)view.FindViewById(Resource.Id.txtConfirmationNumber);
            ImageView qrImage = (ImageView)view.FindViewById(Resource.Id.qrImage);

            if (isFirstClass)
            {
                txtSeatType.Text = "Seat Type: First Class";
                seatTypeName = "First Class";
            }
            else
            {
                txtSeatType.Text = "Seat Type: Business Class";
                seatTypeName = "Business Class";
            }
            string qrCode = fId + "," + tId + "," + fDate + "," + fTime + "," + fName + "," + lName + "," + seatTypeName + "," + con;

            var barcodeWriter = new ZXing.Mobile.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions
                {
                    Width = 600,
                    Height = 600
                }
            };

            var code = barcodeWriter.Write(qrCode);
            qrImage.SetImageBitmap(code);

            txtName.Text = "Name: " + fName + " " + lName;
            txtDate.Text = "Date: " + fDate;
            txtDepartureTime.Text = "Departure Time: " + fTime;
            txtRoute.Text = "From " + fId + " To " + tId;
            txtConNumber.Text = "Confirmation Number: YF" + con;

            if (isFirstClass) { txtSeatType.Text = "Seat Type: First Class"; }
            else { txtSeatType.Text = "Seat Type: Business Class"; }

            return view;
        }
    }
}