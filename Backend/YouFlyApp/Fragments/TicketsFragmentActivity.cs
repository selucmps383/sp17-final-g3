﻿using Android.App;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.App;
using YouFlyModels;
using System.Collections.Generic;
using YouFly.Adapters;
using Newtonsoft.Json;
using RestSharp;
using Android.Widget;

namespace YouFlyApp.Fragments
{
    [Activity(Label = "Tickets")]
    public class TicketsFragmentActivity : FragmentActivity
    {
        private List<Ticket> tickets;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.TicketViewPager);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
            Purchase currentPurchase = JsonConvert.DeserializeObject<Purchase>(Intent.GetStringExtra("purchase"));

            string url = "/api/ticketapi/bypurchaseid/" + currentPurchase.ConfirmationNumber.ToString();
            tickets = new List<Ticket>();

            var client = new RestClient("https://yfapig3.azurewebsites.net");
            RestRequest request = new RestRequest(url, Method.GET);

            request.AddHeader("Content-type", "application/json");

            var response = client.Execute<Purchase>(request);
            var data = JsonConvert.DeserializeObject<IEnumerable<Ticket>>(response.Content);

            foreach (Ticket t in data)
            {
                tickets.Add(t);
            }

            TicketListAdapter adapter = new TicketListAdapter(SupportFragmentManager, tickets);
            viewPager.Adapter = adapter;
        }
    }
}

