#Group 3#

#Instructions:#

To run our Angular app, run the following commands in NPM

npm install angular2-cookie --save

npm install bootstrap@3

Also, our application should be run on ng serve instead of ng build due to complications

Checkout our "Scrum Board" to see what's happening.

###[Scrum Board](https://docs.google.com/document/d/1P3Ns5ie3g_LIkaEKFi8JyN0rRx5LQ0gKUHDuXMuw-dc/edit)