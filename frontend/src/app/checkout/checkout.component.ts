import { CheckoutService } from '../checkout/checkout-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  monthCheck;
  yearCheck;
  constructor(private checkoutService: CheckoutService) { }

  ngOnInit() {
  }
  
}
