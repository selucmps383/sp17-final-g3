import { CheckoutComponent } from './checkout/checkout.component';
import { FlightsListComponent } from './flights-list/flights-list.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LogoutComponent } from './logout/logout.component';
import { PurchaseDetailsComponent } from './purchase-details/purchase-details.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: '/homepage', pathMatch: 'full' },
    { path: 'homepage', component: HomepageComponent },
    { path: 'flights-list', component: FlightsListComponent },
    { path: 'register-user', component: RegisterUserComponent },
    { path: 'sign-in', component: SignInComponent },
    { path: 'logout', component: LogoutComponent },
    { path: 'purchase-details', component: PurchaseDetailsComponent },
    { path: 'purchase-details/:id', component: PurchaseDetailsComponent },
    { path: 'purchase-details/:id/checkout', component: CheckoutComponent },
    { path: 'purchase-details/checkout', component: CheckoutComponent },
    { path: 'purchase-details/:id/thankyou/:id', component: ThankyouComponent},
    { path: 'purchase-details/:id/checkout/thankyou', component: ThankyouComponent},
    { path: 'purchase-details/:id/checkout/:id/thankyou', component: ThankyouComponent},



    //keep '**' this below all other paths//
    { path: '**', component: HomepageComponent },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }