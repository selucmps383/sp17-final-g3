import { CookieService } from 'angular2-cookie/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'YouFly';
  loggedIn: boolean;
constructor(private cookieService: CookieService) { }
  ngDoCheck(){
    if(this.cookieService.get('email') == "0"){
      this.loggedIn = false;
    }
    else{
      this.loggedIn = true;      
    }
  }
}
