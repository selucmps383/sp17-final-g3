export interface RouteList {
    id: string[];
    fromAirportId: string[];
    toAirportId: string[];
    day: string[];
    time: string[];
}