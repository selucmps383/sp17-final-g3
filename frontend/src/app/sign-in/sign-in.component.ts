import { User } from '../user/user';
import { UserService } from '../user/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  user: User = {
    id: null,
    email: "0",
    password: null,
    isAdmin: false
  }
  constructor(private userService: UserService, private cookieService: CookieService,
    private router: Router) { }

  ngOnInit() {
    console.log(this.cookieService.get('email'));
  }

  signIn(email: string, password: string) {
    this.userService.getUserByEmailAndPassword(email, password)
      .subscribe((response: any) => {
        let responseString = JSON.stringify(response);
        this.user = response;
      })
    setTimeout(() => {
      if (this.user.email == "0") {
        document.getElementById("error").style.visibility = "visible";

      }
      else {
        this.cookieService.remove('email');
        this.cookieService.put('email', this.user.email);
        this.router.navigateByUrl('/homepage');
      }
    }, 1000);
  }
}
