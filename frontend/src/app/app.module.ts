import { AirportService } from './airport/airport.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CheckoutService } from './checkout/checkout-service.service';
import { CheckoutComponent } from './checkout/checkout.component';
import { FlightsListComponent } from './flights-list/flights-list.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LogoutComponent } from './logout/logout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PurchaseDetailsComponent } from './purchase-details/purchase-details.component';
import { PurchaseHistoryComponent } from './purchase-history/purchase-history.component';
import { PurchaseInformationComponent } from './purchase-information/purchase-information.component';
import { PurchaseSeatsComponent } from './purchase-seats/purchase-seats.component';
import { PurchaseService } from './purchase/purchase.service';
import { RegisterUserComponent } from './register-user/register-user.component';
import { RouteService } from './route/route.service';
import { SeatTypePipe } from './seat-type.pipe';
import { SignInComponent } from './sign-in/sign-in.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { TicketService } from './ticket/ticket.service';
import { EmailService } from './email.service';
import { UserService } from './user/user.service';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Ng2CompleterModule } from 'ng2-completer';
import { RowPipePipe } from './row-pipe.pipe';



@NgModule({
  declarations: [
    AppComponent,
    CheckoutComponent,
    RegisterUserComponent,
    FlightsListComponent,
    PurchaseSeatsComponent,
    SignInComponent,
    PurchaseInformationComponent,
    PurchaseHistoryComponent,
    HomepageComponent,
    PageNotFoundComponent,
    PurchaseDetailsComponent,
    SeatTypePipe,
    ThankyouComponent,
    LogoutComponent,
    RowPipePipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    Ng2CompleterModule,

  ],
  providers: [UserService, RouteService, AirportService, PurchaseService, TicketService, CheckoutService, RouteService, CookieService, EmailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
