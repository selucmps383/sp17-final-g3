import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PurchaseDetailsComponent } from '../purchase-details/purchase-details.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {

  constructor(private cookieService: CookieService, private router: Router, private activatedRoute: ActivatedRoute) { }
  private subRouteId: any;
  private rId: any;
  ngOnInit() {
    if (this.cookieService.get('email') == "0") {
      this.router.navigateByUrl('/homepage');
    }
    else {
      console.log("logged in");
    }
    this.subRouteId = this.activatedRoute.params.subscribe(params => {
      this.rId = params['id'];
      console.log("RID: " + this.rId);
    });
  }
}
