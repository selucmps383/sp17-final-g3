import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/map';
import { Airport } from '../airport/airport';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AirportService {
  private baseUrl: string = "http://yfapig3.azurewebsites.net";

  constructor(private http: Http) { }

  getAirportByPartial(entry: string): Observable<Airport> {
    return this.http.get(`https://yfapig3.azurewebsites.net/api/airportapi/partial/${entry}`)
      .map((response: Response) => <Airport>response.json());
  }

  searchAirports(term: string): Observable<Airport> {
    return this.http.get(`https://yfapig3.azurewebsites.net/api/airportapi/?name=${term}`)
      .map((response: Response) => <Airport>response.json());
  }

  getIdByAirportName(airport): Observable<Airport> {
    return this.http.get(`https://yfapig3.azurewebsites.net/api/airportapi/byname/${airport}`)
      .map((response: Response) => <Airport>response.json());
  }

  getIdByCityName(city): Observable<Airport> {
    return this.http.get(`https://yfapig3.azurewebsites.net/api/airportapi/bycity/${city}`)
      .map((response: Response) => <Airport>response.json());
  }
}
