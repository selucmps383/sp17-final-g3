export interface Ticket {
    id: number;
    PurchaseId: number;
    IsFirstClass: boolean;
    OccupantFirstName: string;
    OccupantLastName: string;
}