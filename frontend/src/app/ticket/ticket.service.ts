import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Ticket } from './ticket';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SimpleNotificationsModule } from 'angular2-notifications';

@Injectable()
export class TicketService {
    private baseURL: string = 'https://yfapig3.azurewebsites.net';

    constructor(private http: Http){}

    gostTickets(ticket: Ticket, conNum: number): Observable<boolean> {
        return this.http.get(`https://yfapig3.azurewebsites.net/api/ticketapi/newticket/${conNum}&${ticket.OccupantFirstName}&${ticket.OccupantLastName}&${ticket.IsFirstClass}`)
            .map((response: Response) => <boolean>response.json());
    }
}