import { Router } from '@angular/router';
import { User } from '../user/user';
import { UserService } from '../user/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  private user: User;
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() { }

  confirmPassword(email: string, password: string, confirmPassword: string) {
    if (password == confirmPassword) {
      this.submitRegistration(email, password);
    }
    else {
      document.getElementById("passwordDoNotMatch").style.visibility = "visible";
    }
  }

  submitRegistration(email: string, password: string) {
    this.userService.gostRegisterUser(email, password)
      .subscribe((response: any) => {
        this.user = response;
        console.log(response);
        if (response == true) {
          this.router.navigateByUrl('/homepage');
        }
        else {
          document.getElementById("error").style.visibility = "visible";
        }
      })
  }
}

