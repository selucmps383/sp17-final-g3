import { EmailService } from '../email.service';
import { Purchase } from '../purchase/purchase';
import { PurchaseService } from '../purchase/purchase.service';
import { Route } from '../route/route';
import { RouteService } from '../route/route.service';
import { Ticket } from '../ticket/ticket';
import { TicketService } from '../ticket/ticket.service';
import { Component, OnInit, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';

@Component({
  selector: 'app-purchase-details',
  templateUrl: './purchase-details.component.html',
  styleUrls: ['./purchase-details.component.css'],
})
export class PurchaseDetailsComponent implements OnInit {
  private checkingOut: boolean;
  private subRouteId: any;
  private rId: any;
  private fName = '';
  private lName = '';
  private purchase: Purchase;
  private BizSeatsLeft: number;
  private FirstSeatsLeft: number;
  private FirstClassCost: number;
  private BizClassCost: number;
  private numFirstClass: number;
  private numBusClass: number;
  private route: Route;
  private tickets: Ticket[] = [];
  private ticket: Ticket;
  private purchasePrice: number;
  private conNum: number;
  constructor(
    private routeService: RouteService,
    private purchaseService: PurchaseService,
    private ticketService: TicketService,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private router: Router,
    private emailService: EmailService,
  ) { }

  ngOnInit() {
    if (this.cookieService.get('email') == "0") {
      this.router.navigateByUrl('/homepage');
    }
    else {
      console.log("logged in");
    }

    this.subRouteId = this.activatedRoute.params.subscribe(params => {
      this.rId = params['id'];
      console.log("RID: " + this.rId);
    });

    this.purchasePrice = 0;
    this.numBusClass = 0;
    this.numFirstClass = 0;
    this.receiveRoute();
  }

  receiveRoute() {
    this.routeService.getRoute(this.rId)
      .subscribe((response: any) => {
        this.route = response;
        let t;
        for (var i in this.route) {
          t = this.route[i];
        }
        this.BizSeatsLeft = this.route.businessClassSeatCount;
        this.FirstSeatsLeft = this.route.firstClassSeatCount;
        this.BizClassCost = this.route.businessClassCost;
        this.FirstClassCost = this.route.firstClassCost;
      });
  }

  getTicketInfo(firstName: string, lastName: string, isFirstclass: boolean) {
    this.ticket = {
      id: 0,
      PurchaseId: 0,
      OccupantFirstName: "nothing",
      OccupantLastName: "nothing",
      IsFirstClass: false
    };

    this.fName = '';
    this.lName = '';

    this.ticket.id = 0;
    this.ticket.PurchaseId = 0;
    this.ticket.OccupantFirstName = firstName;
    this.ticket.OccupantLastName = lastName;
    this.ticket.IsFirstClass = isFirstclass;

    if (isFirstclass && this.FirstSeatsLeft > 0) {
      this.purchasePrice += this.FirstClassCost;
      this.numFirstClass++;
      this.FirstSeatsLeft--;
      this.tickets.push(this.ticket);
      this.ticket = null;
      this.displayTickets();
    } else if (!isFirstclass && this.BizSeatsLeft > 0) {
      this.purchasePrice += this.BizClassCost;
      this.numBusClass++;
      this.BizSeatsLeft--;
      this.tickets.push(this.ticket);
      this.ticket = null;
      this.displayTickets();
    }
    else {
      console.log("Sorry, there are no more seats available for this Class type");
    }

    return this.tickets;
  }

  clearFields() {
    this.fName = ' ';
    this.lName = ' ';
  }
  displayTickets() {
    this.tickets.forEach(element => {
    });
  }
  completePurchase(tickets: Ticket[]) {
    this.purchase = {
      id: 0,
      price: 0,
      confirmationNumber: 0,
      flightDate: " ",
      routeId: 0,
      userEmail: " "
    };
    this.writePurchase(this.purchase);
    setTimeout(() => {
      if (this.conNum >= 0) {
        this.sendEmail();
        this.writeTickets();
      } else {
        console.log("No es here");
      }
    }, 1000);

  }

  writePurchase(purchase: Purchase) {
    this.purchaseService.gostPurchase(this.rId, this.cookieService.get('email'), this.numFirstClass, this.numBusClass)
      .subscribe((response: any) => {
        this.conNum = response;
      })
    return this.conNum;
  }

  writeTickets() {
    this.tickets.forEach(tick => {
      console.log("Name: " + tick.OccupantFirstName);
      console.log("Con: " + this.conNum);
      this.ticketService.gostTickets(tick, this.conNum)
        .subscribe((response: any) => {
          console.log("Response: " + response);
        })
    });
    this.router.navigateByUrl('/homepage');
  }
  sendEmail() {
    this.emailService.gostEmail(this.cookieService.get('email'), this.conNum)
      .subscribe((response: any) => {
        this.conNum = response;
      })
  }
  isCheckingOut() {
    this.checkingOut = true;
  }
  onCardKey(event: any) {

    //console.log("hey");
    if (event.target.value.length == event.target.maxLength) {
      //console.log("hi");
      event.srcElement.nextElementSibling.focus();
      //event.target.next("card").focus();
    }
  }

  finalize(card1: string, card2: string, card3: string, card4: string, code: string, month: string, year: string, name: string) {
    var isTrue: Boolean;
    isTrue = this.check(card1, card2, card3, card4, code, month, year, name);
    if (isTrue) {
      document.getElementById("send").click();
      //this.checkoutService.addNew("cole.magee@selu.edu");

    } else {
      document.getElementById("error").style.visibility = "visible";
    }
  }

  check(card1: string, card2: string, card3: string, card4: string, code: string, month: string, year: string, name: string) {

    if (card1 == "") {
      return false;
    }
    if (card2 == "") {
      return false;
    }
    if (card3 == "") {
      return false;
    }
    if (card4 == "") {
      return false;
    }
    if (code.length != 3) {
      return false;
    }
    if (Number(year) == 2017 && Number(month) <= 5) {
      return false;
    }

    if (name == "") {
      return false;
    }

    return true;

  }
}

