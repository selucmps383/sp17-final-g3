import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private cookieService: CookieService, private router: Router) { }

  ngOnInit() {
    console.log("Email: " + this.cookieService.get('email'));
    this.cookieService.remove('email');
    this.cookieService.put('email', '0');
    this.router.navigateByUrl('/homepage');
  }

  logout() {
    this.cookieService.remove('email');
    this.cookieService.put('email', '0');
    this.router.navigateByUrl('/homepage');
  }

}
