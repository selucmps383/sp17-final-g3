import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class EmailService {
    private baseUrl: string = "http://yfapig3.azurewebsites.net";

    constructor(private http: Http) { }

    gostEmail(email, conNum) {
        return this.http.get(`${this.baseUrl}/api/userapi/email/${email}&${conNum}`)
            .map((response: Response) => <string>response.json());
    }
}
