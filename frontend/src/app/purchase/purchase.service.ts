import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PurchaseService {
    private baseURL: string = 'https://yfapig3.azurewebsites.net';

    constructor(private http: Http){}

    gostPurchase(routeId: number, userEmail: string, numFirstClass: number, numBusClass: number): Observable<number> {
        return this.http.get(`https://yfapig3.azurewebsites.net/api/purchaseapi/purchase/${routeId}&${userEmail}&${numFirstClass}&${numBusClass}`)
            .map((response: Response) => <number>response.json());
    }
}