export interface Purchase {
    id: number,
    price: number,
    confirmationNumber: number,
    flightDate: string,
    routeId: number,
    userEmail: string
}