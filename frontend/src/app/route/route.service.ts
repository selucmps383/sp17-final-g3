import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Route } from './route';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SimpleNotificationsModule } from 'angular2-notifications';

@Injectable()
export class RouteService {
    private baseURL: string = 'https://yfapig3.azurewebsites.net';

    constructor(private http: Http) { }
    getRoute(id: number): Observable<Route> {
        return this.http.get(`https://yfapig3.azurewebsites.net/api/routeapi/byid/${id}`)
            .map((response: Response) => <Route>response.json());
    }
    getRoutes(): Observable<Route> {
        return this.http.get('https://yfapig3.azurewebsites.net/api/routeapi/')
            .map((response: Response) => <Route>response.json());
    }

    getRouteByToFrom(fromId, toId): Observable<Route> {
        return this.http.get(`https://yfapig3.azurewebsites.net/api/routeapi/findroute/${fromId}&${toId}`)
            .map((response: Response) => <Route>response.json());
    }
    getClosestRoute(fromId, toId) {
        return this.http.get(`http://yfapig3.azurewebsites.net/api/routeapi/findclosest/${fromId}&${toId}`)
            .map((response: Response) => <Route>response.json());
    }
}