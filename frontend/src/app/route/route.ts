export interface Route {
    id: number;
    fromAirportId: number;
    toAirportId: number;
    firstClassSeatCount: number;
    businessClassSeatCount: number;
    firstClassCost: number;
    businessClassCost: number;
    dayOfWeek: string;
    timeOfDay: number;
    flightTime: number;
    flightDistance: number;
    active: boolean;
}