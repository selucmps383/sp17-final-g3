import { CookieService } from 'angular2-cookie/core';
import { Airport } from '../airport/airport';
import { AirportService } from '../airport/airport.service';
import { RouteService } from '../route/route.service';
import { Route } from '../route/route';
import { Component, OnInit } from '@angular/core';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flights-list',
  templateUrl: './flights-list.component.html',
  styleUrls: ['./flights-list.component.css']
})
export class FlightsListComponent implements OnInit {
  private loggedIn: boolean;

  private airport: Airport;
  private airportList: Airport[] = [];
  private dataService: CompleterData;

  private code1: string;
  private code2: string;
  private daySelected: string;

  private airportCitytList: string[] = [];
  private airportCodeList: string[] = [];
  private airportNameList: string[] = [];
  private dataSource: string[] = [];

  private route: Route;
  private routes: Route[];
  private routeString = "";
  private routeStrings: string[] = [];

  private toSearch: string;
  private fromSearch: string;
  private selectEntry = 1;

  private from: string;
  private to: string;
  private day: string;
  private time: string;
  private timeLength: string;
  private routeId: string;

  private fromList: string[] = [];
  private toList: string[] = [];
  private daList: string[] = [];
  private timeList: string[] = [];
  private timeLengthList: string[] = [];
  private routeIdList: string[] = [];

  private searchTypes: any[] = [
    { id: 1, name: 'Airport' },
    { id: 2, name: 'City' },
    { id: 3, name: 'Code' },
  ];

  constructor(private cookieService: CookieService, private router: Router, private airportService: AirportService, private completerService: CompleterService, private routeService: RouteService) { }

  ngOnInit() {
    this.timeConvert();
    if (this.cookieService.get('email') == "0") {
      this.loggedIn = false;
    }
    else {
      this.loggedIn = true;
    }
  }

  onKey(entry: string) {
    if (entry.length >= 1) {
      this.determineSearchType(entry);
    }
  }

  getCookie(key: string) {
    return this.cookieService.get(key);
  }
  determineSearchType(entry) {
    if (this.selectEntry == 1) {
      this.searchAirportNames(entry);
    }
    else if (this.selectEntry == 2) {
      this.searchAirportCities(entry);
    }
    else if (this.selectEntry == 3) {
      this.searchAirportCodes(entry);
    }
    else {
      console.log("Selection Not Found");
    }
  }

  getValueFromSelect(value) {
    if (value == 1) {
      this.selectEntry = 1;
    }
    else if (value == 2) {
      this.selectEntry = 2;
    }
    else {
      this.selectEntry = 3;
    }
  }

  searchAirportNames(entry: string) {
    this.airportService.getAirportByPartial(entry)
      .subscribe((response: any) => {
        this.airportList = response;
        for (var i in response) {
          this.airportList[i] = response[i];
          for (var j in this.airportList[i]) {
            if (j == "name") {
              this.dataSource[i] = this.airportList[i][j];
            }
          }
        }
      })
  }

  searchAirportCities(entry: string) {
    this.airportService.getAirportByPartial(entry)
      .subscribe((response: any) => {
        this.airportList = response;
        for (var i in response) {
          this.airportList[i] = response[i];
          for (var j in this.airportList[i]) {
            if (j == "city") {
              this.dataSource[i] = this.airportList[i][j];
            }
          }
        }
      })
  }

  searchAirportCodes(entry: string) {
    this.airportService.getAirportByPartial(entry)
      .subscribe((response: any) => {
        this.airportList = response;
        for (var i in response) {
          this.airportList[i] = response[i];
          for (var j in this.airportList[i]) {
            if (j == "code") {
              this.dataSource[i] = this.airportList[i][j];
            }
          }
        }
      })
  }

  searchForRoute(from: string, to: string) {
    if (this.selectEntry == 1) {
      this.getCodeByAirportName(from, to);
      setTimeout(() => {
        console.log("Code1: " + this.code1);
        console.log("Code2: " + this.code2);
      }, 100);
    }
    else if (this.selectEntry == 2) {
      this.getCodeByCityName(from, to);
      setTimeout(() => {
        console.log("Code1: " + this.code1);
        console.log("Code2: " + this.code2);
      }, 10);
    }
    else {
      this.code1 = from;
      this.code2 = to;
      setTimeout(() => {
        console.log("Code1: " + this.code1);
        console.log("Code2: " + this.code2);
      }, 10);
    }

    setTimeout(() => {
      this.routeService.getRouteByToFrom(this.code1, this.code2)
        .subscribe((response: any) => {
          this.route = response;
          if (this.route.toString() == "") {
            setTimeout(() => {
              console.log("not found");
            }, 100);
            console.log("not found");

            document.getElementById("error").style.visibility = "visible";
            this.suggestedRoute(from, to);
          }
          else {

            for (var i in response) {
              for (var j in response[i]) {
                console.log("Response: " + this.route[i][j]);
                if (j == "id") {
                  this.routeId = this.route[i][j];
                  this.routeIdList.push(this.route[i][j]);
                }
                if (j == "fromAirportId") {
                  this.from = this.route[i][j];
                  this.fromList.push(this.route[i][j]);
                }
                if (j == "toAirportId") {
                  this.to = this.route[i][j];
                  this.toList.push(this.route[i][j]);
                }
                if (j == "dayOfWeek") {
                  this.day = this.route[i][j];
                  this.daList.push(this.route[i][j]);
                }
                if (j == "timeOfDay") {
                  this.time = this.route[i][j];
                  this.timeList.push(this.route[i][j]);
                }
              }
            }
            this.createRouteString(this.from, this.to, this.day, this.time, this.timeLength);
            this.createRouteStrings(this.fromList, this.toList, this.daList, this.timeList, this.timeLengthList);
          }
        })
      this.timeConvert();
    }, 10);
  }

  getCodeByAirportName(airportName, otherAirportName) {
    let property = "";
    this.airportService.getIdByAirportName(airportName)
      .subscribe((response: any) => {
        property = response;
        for (var i in response) {
          if (i == "code") {
            this.code1 = property[i];
          }
        }
      })

    this.airportService.getIdByAirportName(otherAirportName)
      .subscribe((response: any) => {
        property = response;
        for (var i in response) {
          if (i == "code") {
            this.code2 = property[i];
          }
        }
      })
  }

  getCodeByCityName(airportName, otherAirportName) {
    let property = "";
    this.airportService.getIdByCityName(airportName)
      .subscribe((response: any) => {
        property = response;
        for (var i in response) {
          if (i == "code") {
            this.code1 = property[i];
          }
        }
      })

    this.airportService.getIdByCityName(otherAirportName)
      .subscribe((response: any) => {
        property = response;
        for (var i in response) {
          if (i == "code") {
            this.code2 = property[i];
          }
        }
      })
  }

  suggestedRoute(from, to) {

    let property = "";
    this.routeService.getClosestRoute(from, to)
      .subscribe((response: any) => {
        property = response;
        for (var i in response) {
          if (i == "fromAirportId") {
            this.from = property[i];
          }
          if (i == "toAirportId") {
            this.to = property[i];
          }
        }
        setTimeout(() => {
          this.suggestedRouteReturn(this.from, this.to);
        }, 1000);
      })
  }

  suggestedRouteReturn(from, to) {
    this.routeService.getRouteByToFrom(from, to)
      .subscribe((response: any) => {
        this.route = response;
        for (var i in response) {
          for (var j in response[i]) {
            if (j == "id") {
              this.timeList.push(this.route[i][j]);
            }
            if (j == "fromAirportId") {
              this.fromList.push(this.route[i][j]);
              this.from = this.route[i][j];
            }
            if (j == "toAirportId") {
              this.toList.push(this.route[i][j]);
              this.to = this.route[i][j];
            }
            if (j == "dayOfWeek") {
              this.daList.push(this.route[i][j]);
              this.day = this.route[i][j];
            }
            if (j == "timeOfDay") {
              this.timeList.push(this.route[i][j]);
              this.time = this.route[i][j];
            }
          }
          this.createRouteStrings(this.fromList, this.toList, this.daList, this.timeList, this.timeLengthList);
        }
      })
  }

  createRouteString(from, to, day, time, timeLength) {
    let route = day + " " + time + ": " + from + " to " + to + " ";
    this.addRouteToRouteString(route);
  }

  createRouteStrings(from, to, day, time, timeLength) {
    console.log("creatRoute hit");
    let routeString;
    for (var i = 0; i < from.length; i++) {
      routeString = day[i] + " " + time[i] + ": " + from[i] + " to " + to[i] + " ";
      this.addRouteToRouteString(routeString);
    }
  }

  checkLoggedIn() {
    if (!this.loggedIn) {
      this.router.navigateByUrl('/sign-in');
    }
  }

  timeConvert() {

    this.timeList.forEach(element => {
      let time = +element;
      if (time == 0) {
        element = "12 AM"
      }
      else if (time > 12) {
        if (time == 13) {
          element = "1:00 PM";
        }
        if (time == 14) {
          element = "2:00 PM";
        }
        if (time == 15) {
          element = "3:00 PM";
        }
        if (time == 16) {
          element = "4:00 PM";
        }
        if (time == 17) {
          element = "5:00 PM";
        }
        if (time == 18) {
          element = "6:00 PM";
        }
        if (time == 19) {
          element = "7:00 PM";
        }
        if (time == 20) {
          element = "8:00 PM";
        }
        if (time == 21) {
          element = "9:00 PM";
        }
        if (time == 22) {
          element = "10:00 PM";
        }
        if (time == 23) {
          element = "11:00 PM";
        }
        if (time == 24) {
          element = "12:00 PM";
        }
      }
      else {
        element = element + ":00 AM"
      }
    });
  }


  addRouteToRouteString(route: string) {
    console.log("not found");
    this.routeStrings.push(route);
  }
}
