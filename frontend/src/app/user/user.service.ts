import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/map';
import { User } from './user';
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class UserService {
  private baseUrl: string = "http://yfapig3.azurewebsites.net";

  constructor(private http: Http) { }

   gostRegisterUser(email: string, password: string) {
    return this.http.get(`${this.baseUrl}/api/userapi/register/${email}&${password}`)
      .map((response: Response) => <User>response.json());
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get(`${this.baseUrl}/api/userapi/byid/${id}`)
      .map((response: Response) => <User>response.json());
  }

  getUserByEmail(email: string) {
    return this.http.get(`${this.baseUrl}/api/userapi/byemail/${email}`)
      .map((response: any) => <User>response.json());
  }

  getUserByEmailAndPassword(email: string, password: string): Observable<User> {
    return this.http.get(`${this.baseUrl}/api/userapi/authenticate/${email}&${password}`)
      .map((response: Response) => <User>response.json());
  }
}
