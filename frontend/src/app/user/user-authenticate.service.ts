import { User } from './user';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
  private baseUrl: string = "http://yfapig3.azurewebsites.net";

  constructor(private http: Http) { }

  getUserByEmailAndPassword(email: string, password: string): Observable<User> {
    return this.http.get(`${this.baseUrl}/api/userapi/authenticate/${email}&${password}`)
      .map((response: Response) => <User>response.json());
  }
}
